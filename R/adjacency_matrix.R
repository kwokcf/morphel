create_adjacency_matrix <- function(model_var) {
  l <- length(model_var)
  m0 <- matrix(0, ncol = l, nrow = l)
  rownames(m0) <- colnames(m0) <- model_var
  m0
}

update_adjacency_matrix <- function(m0, lhs, rhs) {
  m0[lhs, rhs] <- -1  
  m0
}

switch_view <- function(m0) {
  t(-m0)
}
